---
title: Home
nav_order: 1
description: "Full Stack courseware."
permalink: /
---

# Full Stack Courseware

## Introduction

This course demonstrates the [Twelve-Factor App](https://12factor.net/) methodology via a full stack application. It makes use of Docker to build a React web application and a GraphqQL server, and makes use of prebuilt timeline (Postgres) docker image.

This course can be run on a Windows, Linux or MacOs machine and is composed of five activities and all these activities need to be run consecutively apart from the _Mock the Server_ activity, which is independent of the others. 

There are 3 Gitlab projects associated with this course: 
[Stack Up Administration](https://gitlab.com/JimRBannister/stack-up-administration), 
[Stack Up GraphQL](https://gitlab.com/JimRBannister/stack-up-graphql), and
[Stack Up React](https://gitlab.com/JimRBannister/stack-up-react)


## Activities

- [Deployments and configurations](#deployments-and-configurations)
- [Mock the Server!](#mock-the-server)
- [Securely store passwords used by the Server](#securely-store-passwords-used-by-the-server)
- [Test our Server with Jest](#test-our-server-with-jest)
- [Develop a React app with ChartJS in no time](#develop-a-react-app-with-chartjs-in-no-time)

***

## Installation Requirements

 
### Docker Desktop or Docker on Linux.
 
[Docker install instructions](https://docs.docker.com/get-docker/)
 
Check the installation works by calling the following from the command line

```bash
docker swarm init
```

### Node and npm
 
[Node and npm installation instructions](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm#using-a-node-version-manager-to-install-node-js-and-npm)

Use nvm (node version manager) to manage your node and npm installations.
 
A simple example of using node and npm is given below. This is not necessary for the course. It is provided for future reference.
 
[Simple application using node and npm](https://expressjs.com/en/starter/hello-world.html)
 

### yarn

npm is required first before installing yarn. This is a good way to check the npm installation too.

npm install -g yarn
 

### git

Please install git.
 
[install git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
 


 
### curl

Check you can use the command curl on your machine. It may already be available.

[curl installation instructions](https://everything.curl.dev/get)
 
[Windows technical information note](https://techcommunity.microsoft.com/t5/containers/tar-and-curl-come-to-windows/ba-p/382409)

Try out curl

```bash
 curl --url date.jsontest.com    

```

### Trouble Shooting
In some cases a command may require administrator (root) privileges.

Put sudo in front of the command, and enter your password when prompted.

***

## Deployments and configurations

This first activity deploys existing docker images. Please go to the [Stack Up Administration](https://gitlab.com/JimRBannister/stack-up-administration)
to follow this activity.


## Mock the Server

Create a new project called mock-graphql in GitLab against your own GitLab account 
[GitLab](https://gitlab.com/)


Create a new branch called master. This branch will reflect the source code deployed in production.

Use the "Clone" button to take a copy of the _repository url_.

From your own machine's command window

```bash
git clone <_respository url_>
```


change directory to the new sub-directory just created by _git clone_

For example

```bash
cd mock-graphql
```

create a new branch for our initial changes
```bash
git checkout -b dev
```
This dev branch will contain our completed features and fixes. This dev branch is the working tree for our source repository.

All new development is undertaken within a feature branch. Feature branches are always branched off of the dev branch and prefixed with a feature slash. 

Push the new branch upstream so it also held with the GitLab repository 

```bash
git push -u origin dev
```

the command will return 
Branch 'dev' set up to track remote branch 'dev' from 'origin'.

```bash	
git checkout -b feature/setup dev
```	

We will now create 4 new files in this directory so that we end up with a  working mock. 

Create a new package file via the npm utility. Use the defaults when prompted.

```bash
npm init 
```	

Install the apollo server package. Only one package is needed for your GraphQL server.
```bash
npm install apollo-server
```

edit the package.json file and add the following before the last line (i.e. so that will enclosed by the existing curly brackets)
```
	 , "scripts": {
	    "start": "node index.js"
	  }
```

create a new .gitignore file (this file is used by git so that any directories or files you don't wish to hold in the repository can be ignored) with the contents as follows:

```
	node_modules/
```


create an index.js with the following contents
```
	const { ApolloServer, gql } = require('apollo-server');
	
	const typeDefs = gql`
	 type Temperature {
	    location: String
	    temperature:Float
	    recordedat: Int
	  }
	  type Query {
	    allTemperatures: [Temperature]
	  }
	  type Mutation {
	    addTemperature(location: String!, temperature: Float!): Boolean
	  }
	`;
	
	const resolvers = {
	  Query: {
	    recentTemperatures: () => 'Resolved'
	  },
	  Mutation: {
	    addTemperature: (parent, args, context) => {return true}
	  }
	};
	
	const server = new ApolloServer({
	  typeDefs,
	  mocks:true
	});
	
	server.listen().then(({ url }) => {
	  console.log(`GraphQL Server is ready at ${url}`)
	});
	
```	

Check the correct apostrophe is used in the file. There can be issues with character conversions. 


Now let's fire up the server!! 

```bash
npm start
```

After a successful start you will see the message starting with 
```
GraphQL Server is ready at http://localhost:4000/

```
	
Abort the service by using  CTRL & c  together when finished.
	

Add the files to the feature branch after a successful test

```bash
git add index.js package.json yarn.lock .gitignore
```
	
	
Commit your changes
```bash	
git commit -m "setup graphql mock server"
```
	
Now let's merge our changes from the feature branch into dev

```bash
git checkout dev
git merge feature/setup
git push
```
	
Ensure our local dev branch is up to date!

```bash
git rebase
```

dev has now been updated with the feature branch.

Now we wish our new feature to be incorporated into the master branch to ensure that our development and production code bases do not diverge too much and so maintain dev/prod partity.

Attempt to checkout the master, merge the changes from dev, and push those changes to the repository.

Check your changes are reflected in the master branch via your GitLab project web page.

Congratulations. The Mock the Server activity has been completed.


## Securely store passwords used by the Server

This activity includes building a GraphQL docker image and deploying this server with a database service.  Please go first to [Stack Up Graphql](https://gitlab.com/JimRBannister/stack-up-graphql) and then  [Stack Up Administration](https://gitlab.com/JimRBannister/stack-up-administration)
to follow this activity.

## Test our Server with Jest

This activity covers testing with Docker. It includes building a GraphQL Test image and deploying this server with a test database service.  Please go first to [Stack Up Graphql](https://gitlab.com/JimRBannister/stack-up-graphql) and then  [Stack Up Administration](https://gitlab.com/JimRBannister/stack-up-administration)
to follow this activity.

## Develop a React App with ChartJS in no time

This activity includes building a React docker image and deploying this Web application service with the GraphQL and database services.  Please go first to [Stack Up React](https://gitlab.com/JimRBannister/stack-up-react) and then  [Stack Up Administration](https://gitlab.com/JimRBannister/stack-up-administration)  
to follow this activity.

Your feedback on this course will be welcomed. 
